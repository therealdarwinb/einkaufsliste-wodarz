# Xenia Wodarz
## Lebenslauf

Name: Xenia Wodarz

Anschrift: Biberbau 1 78120 Furtwangen im Schwarzwald

Geburtsdatum: 01.01.2000

### _Bildung_
 * 2006-2010 Grundschule
 * 2010-2018 Gymnasium
 * seit 2018 Hochschule

### _Arbeitserfahrung_
 * seit 2006 Aushilfe in Hotel Mama

### _Besondere Faehigkeiten_
 * Prokrastinieren bis zum Ende
 * ++nicht++ mit Git arbeiten

